#  Dialogs

> Dialogs 对话框主要用于通知用户，汇报状态， 告知详情，有时对话框里会包含一些任务和操作。



### Usage 使用

Dialogs是一种模态对话框，对话框出现的时候，蒙层下方的界面不可操作.   
需要用户执行确认，取消，选择，或者输入之后，对话框才会消失。

对话框的提示性强，对用户是比较大的打扰。查看其它两种能表达 "提示"的 组件

[Tips]()  
[Toast]()

&nbsp;

| **组件** | **提示能力** | **交互** |  
|:------:|:------:|:------:|  
|Dialogs|强|需要用户操作才能消失，蒙层下方不可操作|  
|Tips|中|需要用户操作才能消失，但不会影响其它界面|  
|Toast|弱|2s内自动消失|  

&nbsp;


### Types 种类
&nbsp;

##### Alert Dialog

警示型对话框用来通知用户，汇报状态，或者2次确认用户的操作意向。

<div><img width="960" height="540" src="Asset/Images/Dialogs/Dialogs_Type_Alert.png"/></div>


&nbsp;


##### Passive Dialog

单向信息对话框，也叫做 "消极” 对话框。它的作用仅用于告知用户信息，不会指引用户进行别的操作。

<div><img width="960" height="540" src="Asset/Images/Dialogs/Dialogs_Type_Passive.png"/></div>

&nbsp;

##### Action Dialog

操作型对话框里包含了任务和操作，通常需要用户做出选择，或者输入内容。

<div><img width="960" height="540" src="Asset/Images/Dialogs/Dialogs_Type_Action.png"/></div>

&nbsp;

* * *

&nbsp;

&nbsp;

### Anatomy 解构

<div><img width="960" height="540" src="Asset/Images/Dialogs/Dialogs_Anatomy.png"/></div>

&nbsp;

1. Container 背景
2. Title 标题 (Optional）
3. Text 文本 (Optional）
4. Buttons 按钮
5. Mask 蒙层

&nbsp;

* * *

&nbsp;

&nbsp;
### Buttons 按钮

如果在按钮的文本太长，导致按钮无法水平排列的情况下，将按钮从上往下排列，向右对齐。其中 "确认” 按钮应该位于 “取消” 按钮的上方。

<div><img width="960" height="540" src="Asset/Images/Dialogs/Dialogs_Buttons.png"/></div>

&nbsp;

* * *

&nbsp;

&nbsp;

### Gesture 手势
&nbsp;

##### Interaction 交互

Dialogs是模态的，它的出现打断了用户的进程，强制用户优先处理当前Dialogs。  
其中Passive和Action形式的Dialogs，可以通过点击空白区域关闭，但Alert形式的Dialogs则必须点击取消按钮才能关闭。
&nbsp;

&nbsp;

##### Dismissing Dialog 关闭对话框

可以通过以下3种操作关闭对话框：
&nbsp;
1. 点击空白区域

2. 点击取消按钮

3. 点击手机系统的回退键

&nbsp;
需要注意的是，Alert形式的Dialogs必须点击取消按钮才能关闭。

&nbsp;

&nbsp;


##### Position 位置

Dialogs的位置在界面的最上层，Dialogs不会被其他界面元素所遮挡。

&nbsp;
&nbsp;

##### Scrolling 滚动

有些Dialogs里的文字很长，或者包含了很长的列表，这时需要允许Dialogs内容可滚动。  
需要注意的是，Dialogs的标题固定在顶部，按钮固定在底部，只有中间的内容部分是可以滚动的。

<div><img width="960" height="540" src="Asset/Images/Dialogs/Dialogs_Scrolllable.png"/></div>

&nbsp;

* * *

&nbsp;

&nbsp;
### Alert Dialog 警示型对话框

警示型对话框用来通知用户，汇报状态，或者2次确认用户的操作意向。

警示型对话框通常有2个操作按钮，必须通过点击其中一个按钮，对话框才会消失
&nbsp;

##### Ordinary 普通

一般情况下，普通的对话框包含标题，文本说明，取消按钮，和执行按钮。  
标题内容为该对话框内容的精炼信息，标题内容需要清晰，直接，简短，不含修辞藻饰，文本说明则用来辅助表达。

<div><img width="960" height="430" src="Asset/Images/Dialogs/Dialogs_Alert_Ordinary.png"/></div>

&nbsp;

##### Strong 强烈

在表达强烈警示的情况下，该对话框仅使用标题字号传达内容，标题内容需要清晰，直接，不含修辞藻饰，标题内容要尽可能简短。

<div><img width="960" height="430" src="Asset/Images/Dialogs/Dialogs_Alert_Strong.png"/></div>

&nbsp;


##### Mute 弱化

在对话框文本语气柔和，警示性不强烈的情况下，可以抛弃标题，仅采用文本说明。

<div><img width="960" height="430" src="Asset/Images/Dialogs/Dialogs_Alert_Mute.png"/></div>

&nbsp;

* * *

&nbsp;

&nbsp;
### Passive Dialog 单向信息对话框

单向信息对话框，也叫做 "消极” 对话框。它的作用仅用于告知用户信息，不会指引用户进行别的操作。  
这种对话框可以承载较长的文本信息。

单向信息对话框通常不包含 "取消" 按钮，可以通过点击 "确认" 按钮，或者点击空白区域关闭对话框。

<div><img width="960" height="540" src="Asset/Images/Dialogs/Dialogs_Passive.png"/></div>

&nbsp;

* * *

&nbsp;

&nbsp;
### Action Dialog 操作型对话框

操作型对话框里包含了任务和操作，通常需要用户做出选择，或者输入内容。

操作型对话框一般不需要 "底部文本按钮”，通过点击空白区域即可关闭对话框。
&nbsp;

##### Immediate 即时选择

在需要用户做出选择的情况下，即时选择型的对话框包含标题，和列表。  
标题内容为目的明确的祈使句，列表是层级并列的选项，需要用户通过点击做出单选。

<div><img width="960" height="430" src="Asset/Images/Dialogs/Dialogs_Action_Immediate.png"/></div>

&nbsp;

<div><img width="960" height="430" src="Asset/Images/Dialogs/Dialogs_Action_Immediate_close.png"/></div>
&nbsp;


##### Text Field 输入框

在需要用户输入信息的情况下，对话框可以包含输入框。

<div><img width="960" height="430" src="Asset/Images/Dialogs/Dialogs_Action_Text Field.png"/></div>

&nbsp;

* * *

&nbsp;

&nbsp;
### Color 颜色

<div><img width="960" height="630" src="Asset/Images/Dialogs/Dialogs_Color.png"/></div>
&nbsp;


| **元素** | **色值** |
| :---: | :----: |
| Container 背景 | block_card |
| Title 标题 | text_h1 |
| Text 文本 | text_h2 |
| Buttons 按钮 | text_highlight |
| Mask 蒙层 | bg_mask |

&nbsp;

* * *

&nbsp;

&nbsp;
###  Spec 标注
&nbsp;

##### Alert Dialog

<div><img width="960" height="430" src="Asset/Images/Dialogs/Dialogs_Alert_Spec.png"/></div>
&nbsp;

##### Passive Dialog

<div><img width="960" height="540" src="Asset/Images/Dialogs/Dialogs_Passive_Spec.png"/></div>
&nbsp;

##### Action Dialog

<div><img width="960" height="540" src="Asset/Images/Dialogs/Dialogs_Action_Spec.png"/></div>
&nbsp;

* * *

&nbsp;

&nbsp;
### Typography 字体

| **Category层级** |  **Font**  | **Weight** | **Size** | **Case大小写** |
| :--------------: | :--------: | :--------: | :------: | :------------: |
|    Title 标题    | Din - 2014 | Demi-Bold  |    60    |   首字母大写   |
|    Text 文本     | Din - 2014 |  Regular   |    42    |   首字母大写   |
|   Buttons 按钮   | Din - 2014 | Demi-Bold  |    42    |     全大写     |


&nbsp;

* * *

&nbsp;

&nbsp;
### Shape 形状

<div><img width="960" height="430" src="Asset/Images/Dialogs/Dialogs_Shape.png"/></div>
&nbsp;


| **Category层级** | **Attribute** | **Value** |
| :--------------: | :-----------: | :-------: |
|  Container 背景  |     对象      |    4dp    |

