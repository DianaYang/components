# README - 文档说明

## 目录

- [概述](#概述)
- [组件索引](#组件索引)
- [阅读说明](#阅读说明)
- [标注说明](#标注说明)
- [问题反馈](#问题反馈)
- [最近更新](#最近更新)

## 组件索引

### 基本元素

- [Border - 边框](./border)
- [Color - 颜色](./color)
- [Icon - 图标](./icon)
- [Layout - 布局](./layout)
- [Opacity - 不透明度](./opacity)
- [Shadow - 阴影](./shadow)
- [States - 状态](./states)
- [Typography - 文字排版](./typography)

### 组件

- [Button - 按钮](./button)

  [![Dialogs](Asset/Images/Home/Home_link_Dialogs.png)](./Dialogs.md)


- [List - 列表](./list)

- [List control - 列表控制器](./list_control)

- [Menu - 菜单](./menu)

- [Notification - 通知](./notification)

- [Popover - 弹出层](./popover)

- [Panel - 面板](./panel)

- [Select - 选择器](./select)

- [Selection control - 单选和多选](./selection_controls)

- [Slider - 滑块](./slider)

 [![Tab](Asset/Images/Home/Home_link_Tab.png)](./Tab.md)

- [Template - 模版](./template)

- [Text field - 输入框](./text_fields)

- [Toolbar - 工具栏](./toolbars)

- [Tooltips - 工具提示](./tooltips)