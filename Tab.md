# Tabs

> Tabs用来切换不同的页面, 或者切换不同的数据内容





### Types 种类

##### Mono Space

“等分型” Tabs会在屏幕宽度内完全展示, 其子Tab宽度为 “屏幕宽度/Tab数量” .   
“等分型” Tabs位置固定, 子Tab数量为2到4个, 一般来说不会超过4个.
<div><img width="960" height="540" src="Asset/Images/Tab/Tab_MonoSpace.png"/></div>


&nbsp;

##### Flexible

“灵活型” 的Tab没有固定宽度. 每个子Tab的宽度, 由文本长度和Padding相加得出.当子Tab的个数增多, 直到超出屏幕宽度时, 该Tab由固定变为可滚动.     
右边超出屏幕宽度的子Tab, 可以通过滚动手势展现.默认情况下, 此类型的Tab是从左往右排列的. 

<div><img width="960" height="540" src="Asset/Images/Tab/Tab_Flexible.png"/></div>

&nbsp;

* * *

&nbsp;

&nbsp;

### Anatomy 解构

<div><img width="960" height="540" src="Asset/Images/Tab/Tab_Anatomy.png"/></div>

1. Container 背景
2. Indicator 滑块
3. Tab Item 子tab
4. Active Text 选中文字
5. Resting Text 普通文字
6. Line 分割线

&nbsp;

* * *

&nbsp;

&nbsp;

### Text Label 文本

Tab文本应该单行显示. 
标题文本有两种, 第一种是应用内定义的标题文本, 该文本由产品设计决定, 产品文案上应该确保文本可读性强且长度合适;   
第二种是用户自定义的标题, 该文本由用户自主输入, 在产品的交互设计上, 应该对输入长度进行一定的限制.   

<div><img width="960" height="430" src="Asset/Images/Tab/Text_Label.png"/></div>

1. 不可以折行
2. 不可以缩小文本字号
3. 不可以用 “…” 缺省内容

&nbsp;

* * *

&nbsp;

&nbsp;

### Gesture 手势

##### Tap a Tab 点击

所有的Tab都可以通过点击进行切换, 点击范围为该Tab所占的整块区域

<div><img width="960" height="430" src="Asset/Images/Tab/Tab_TapATab.png"/></div>

&nbsp;

##### Swipe within the Content Area 滑动页面

###### 顶部Tabs

顶部Tabs导领整屏内容, 通过横向滑动内容区域的屏幕, 可以切换到上一个或者下一个Tab. 

<div><img width="960" height="540" src="Asset/Images/Tab/Tab_Swipe.png"/></div>

&nbsp;

###### 页面中部Tabs

页面中部的Tabs仅导领一定区域的内容, 只能通过点击手势进行切换.

<div><img width="960" height="430" src="Asset/Images/Tab/Tab_Tap.png"/></div>


&nbsp;

* * *

&nbsp;

&nbsp;

### States 状态

##### 选中的 Tab

<div><img width="960" height="270" src="Asset/Images/Tab/Tab_States_01.png"/></div>

1. 选中Tab - 正常态
2. 选中Tab - 点击态

&nbsp;
##### 未选中的 Tab

<div><img width="960" height="270" src="Asset/Images/Tab/Tab_States_02.png"/></div>

3. 未选中Tab - 正常态
4. 未选中Tab - 点击态


&nbsp;

* * *

&nbsp;

&nbsp;

### Color

<div><img width="960" height="540" src="Asset/Images/Tab/Tab_Color.png"/></div>

| **元素** | **色值** |
| :---: | :----: |
| Tab背景 | block_bg |
| 选中态文字 | text_highlight |
| 选中态下划线 | line_clickable|
| 未选中态文字 | text_h3 |
| Tab分割线 | line_separator |


&nbsp;

* * *

&nbsp;

&nbsp;

### Typography


| **Category层级** | **Font** | **Weight** | **Size** | **Case大小写** |
| :------: | :------: | :------: | :------: | :------: |
| Title | Din - 2014 | Regular | 42 | 首字母大写 |

&nbsp;

* * *

&nbsp;

&nbsp;

###  Spec

<div><img width="960" height="540" src="Asset/Images/Tab/Tab_Spec.png"/></div>

